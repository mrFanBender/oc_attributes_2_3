<?php

class ModelExtensionModuleAttributesWidget extends Model
{
    public function getWidgetsByGroupIds($groupIds, $languageId, $productId)
    {
        $groupIdsImplode = implode(',', $groupIds);

        $widgets = $this->db->query(
            sprintf("SELECT awd.widget_id, `language_id`, `name`, aw.attribute_group_id, aw.sort_order, aw.status 
                FROM `%sattributes_widget_description` AS awd 
                LEFT JOIN `%sattributes_widget` AS aw
                ON aw.widget_id = awd.widget_id
                WHERE awd.language_id = %s AND aw.attribute_group_id IN (%s) AND aw.status = 1 
                ORDER BY sort_order ASC", DB_PREFIX,DB_PREFIX, $languageId, $groupIdsImplode))->rows;

        foreach($widgets as &$widget) {
            $attributes = $this->db->query(
                sprintf("SELECT awa.*, ad.name, UPPER(pa.text) text, at.sort_order FROM `%sattributes_widget_attr` AS awa 
                    LEFT JOIN `%sattribute_description` AS ad
                        ON awa.attribute_id = ad.attribute_id
                    LEFT JOIN `%sattribute` AS at
                        ON at.attribute_id = awa.attribute_id    
                    LEFT JOIN `%sproduct_attribute` AS pa
                        ON awa.attribute_id = pa.attribute_id
                    WHERE `widget_id` = %s 
                        AND pa.language_id = %s 
                        AND pa.product_id = %s 
                        AND ad.language_id = %s 
                     ORDER BY at.sort_order ASC",
                DB_PREFIX, DB_PREFIX, DB_PREFIX, DB_PREFIX, $widget['widget_id'], $languageId, $productId, $languageId))->rows;
            $widget['attributes'] = $attributes;
        }

        return $widgets;
    }

}