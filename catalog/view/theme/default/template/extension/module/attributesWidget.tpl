<?php foreach ($widgets as $widget): ?>
<h3 class="options-header"><?= $widget['name'] ?></h3>
<div class="row attributes-widget">
    <?php $i = 0; ?>
    <?php foreach($widget['attributes'] as $attribute): ?>
    <?php if($attribute['active'] == 1): ?>
    <?= $i==0 ? "<div class='row'>" : "" ?>
    <div class="col-xs-6 col-sm-6">
        <div class="col-sm-8 col-xs-10">
            <p><?php echo $attribute['name'] ?></p>
        </div>
        <div class="col-sm-4 col-xs-2">
            <p><?php echo $attribute['text'] == 'НЕТ' ? "<span class='glyphicon glyphicon-remove red'></span>" : "<span class='glyphicon glyphicon-ok'></span>" ?><p>
        </div>
    </div>
    <?php $i++; ?>
    <?php endif; ?>
    <?php if($i == 2) {
                    $i = 0;
                    echo "</div>";
    } ?>
    <?php endforeach; ?>
</div>
<?php endforeach; ?>
