<?php

class ControllerExtensionModuleAttributesWidget extends Controller
{
    public function getWidget($data)
    {
        $this->load->model('setting/setting');

        if(!$this->config->get('attributesWidget_status')) {
            return;
        }

        $this->document->addStyle('/catalog/view/theme/default/stylesheet/extension/module/attributesWidget.css');

        //$this->document->addScript('/catalog/view/theme/default/javascript/extension/module/attributesWidget.js');

        $this->load->model('extension/module/attributesWidget');

        $languageId = $data['language_id'];
        $productId = $data['product_id'];
        $data['widgets'] = array();
        $groupIds = array();
        foreach ($data['groups'] as $attributeGroup) {
            array_push($groupIds, $attributeGroup['attribute_group_id']);
        }
        if($groupIds) {
            $data['widgets'] = $this->model_extension_module_attributesWidget->getWidgetsByGroupIds($groupIds, $languageId, $productId);
        }

        return $this->load->view('extension/module/attributesWidget', $data);
    }


}