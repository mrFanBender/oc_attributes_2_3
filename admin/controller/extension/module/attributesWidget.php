<?php

/**
 * Class ControllerExtensionModuleAttributesWidget
 */
class ControllerExtensionModuleAttributesWidget extends Controller
{
    private $error = array();

    private $data = array();
    /**
     * Module Settings
     *
     * @var array
     */
    private $settings = array();

    public function install()
    {
        $this->load->model('setting/setting');
        $this->load->model('extension/module/attributesWidget');
        //Подготовка БД для работы модуля
        $this->model_extension_module_attributesWidget->createTables();

        //инициализация настроек модуля
        $settings = $this->model_setting_setting->getSetting('attributesWidget');
        $settings['attributesWidget_show_menu'] = 1;
        $settings['attributesWidget_active_link'] = 0;
        $settings['attributesWidget_status'] = 0;
        $this->model_setting_setting->editSetting('attributesWidget', $settings);
    }

    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->load->model('extension/module/attributesWidget');

        $this->model_extension_module_attributesWidget->dropTables();

        $settings = $this->model_setting_setting->getSetting('attributesWidget');
        $settings['attributesWidget_show_menu'] = 0;
        $settings['attributesWidget_active_link'] = 0;
        $settings['attributesWidget_status'] = 0;
        $this->model_setting_setting->editSetting('attributesWidget', $settings);
    }

    public function index()
    {
        $this->load->model('setting/setting');
        $this->load->model('extension/module/attributesWidget');
        $this->load->model('catalog/attribute_group');
        $this->load->model('catalog/attribute');
        $this->load->language('extension/module/attributesWidget');
        $this->data['language_id'] = $this->config->get('config_language_id');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->addStyle('/admin/view/stylesheet/extension/module/attributesWidget.css');
        $this->document->addStyle('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
        $this->document->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
        $this->document->addScript('/admin/view/javascript/extension/module/attributesWidget.js');

        $this->settings = $this->model_setting_setting->getSetting('attributesWidget');
        $this->data['module_settings'] = $this->settings;

        $this->data['status'] = $this->data['module_settings']['attributesWidget_status'];
        $this->data['statuses'] = $this->language->get('status');

        //Загрузка текстовых переменных
        $this->data['text'] = array();

        $textParameters = array(
            'heading_title',
            'button_cancel',
            'button_save',
            'button_delete',
            'button_create_new_widget',
            'text_home',
            'text_extension',
            'text_status',
            'text_new_widget',
            'text_exist_widgets',
        );
        foreach ($textParameters as $textParameter) {
            $this->data['text'][$textParameter] = $this->language->get($textParameter);
        }

        //хлебные крошки
        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
            'separator' => ' / ',
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true),
            'separator' => ' / ',
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/attributesWidget', 'token=' . $this->session->data['token'], true),
            'separator' => ' / ',
        );

        //Ссылки
        $data['action'] = $this->url->link('extension/module/featured', 'token=' . $this->session->data['token'], true);

        $this->data['index'] = $this->url->link(
            'extension/module/attributesWidget',
            'token=' . $this->session->data['token'], true
        );
        $this->data['cancel'] = $this->url->link(
            'extension/extension',
            'token=' . $this->session->data['token'] . '&type=module', true
        );

        $this->data['save'] = $this->url->link(
            'extension/module/attributesWidget/save',
            'token=' . $this->session->data['token'], true
        );

        $this->data['create_new_widget'] = $this->url->link(
            'extension/module/attributesWidget/create',
            'token=' . $this->session->data['token'], true
        );

        $this->data['get_attributes_list'] = $this->url->link(
            'extension/module/attributesWidget/getAttributesByGroupId',
            'token=' . $this->session->data['token'], true
        );

        //Errors
        $this->data['error_warnings'] = array();
        if(isset($this->session->data['errors'])){
            $this->data['error_warnings']=$this->session->data['errors'];
            $this->session->data['errors'] = array();
        }

        //Info Messages
        $this->data['info_messages'] = array();
        if(isset($this->session->data['success'])){
            $this->data['info_messages'] = $this->session->data['success'];
            $this->session->data['success'] = array();
        }
        $this->data['attribute_groups'] = $this->model_catalog_attribute_group->getAttributeGroups();
        $this->data['new_widget_attributes'] = array();
        if ($this->data['attribute_groups']) {
            $this->data['new_widget_attributes'] = $this->model_catalog_attribute->getAttributes(array('filter_attribute_group_id' => $this->data['attribute_groups'][0]['attribute_group_id']));
        }

        $this->data['widgets'] = $this->model_extension_module_attributesWidget->getWidgets($this->data['language_id']);
        foreach($this->data['widgets'] as &$widget) {
            $widget['delete_link'] = $this->url->link(
                'extension/module/attributesWidget/delete',
                'widget_id='.$widget['widget_id'].
                '&token=' . $this->session->data['token'], true
            );
            $attributes = $this->model_catalog_attribute->getAttributes(array('filter_attribute_group_id' => $widget['attribute_group_id']));
            foreach($widget['attributes'] as &$widgetAttribute) {
                foreach($attributes as $attribute) {
                    if($widgetAttribute['attribute_id'] == $attribute['attribute_id'])
                        $widgetAttribute['name'] = $attribute['name'];
                }
            }
        }

        //Шаблон
        $this->load->model('design/layout');
        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/attributesWidget', $this->data));
    }

    public function create()
    {
        $this->load->model('catalog/attribute_group');
        $this->load->model('catalog/attribute');
        $languageId = $this->config->get('config_language_id');

        if(!$this->validate()) {
            foreach ($this->data['errors'] as $error) {
                $this->session->data['errors'][] = $error;
            }
            $this->redirectToMethod('index');
        }

        $attributeCategoryId = $this->request->post['new_widget_attribute_category'];
        $attributeCategory = $this->model_catalog_attribute_group->getAttributeGroupDescriptions($attributeCategoryId);
        $attributes = $this->request->post['attributes'];
        $sortOrder = (int)$this->request->post['new_widget_sort_order'];
        $widgetStatus = $this->request->post['new_widget_status'];
        $widgetName = $this->request->post['new_widget_name'] ? $this->request->post['new_widget_name'] : $attributeCategory[$languageId]['name'];

        $prepareAttributes = $this->model_catalog_attribute->getAttributes(array('filter_attribute_group_id' => $attributeCategoryId));

        foreach ($prepareAttributes as &$prepareattribute) {
            $prepareattribute['active'] = 0;
             foreach($attributes as $key => $attribute) {
                if ($prepareattribute['attribute_id'] == $key)
                    $prepareattribute['active'] = 1;
            }
        }

        $this->load->model('extension/module/attributesWidget');
        $widgetId = $this->model_extension_module_attributesWidget->createWidget($attributeCategoryId, $prepareAttributes, $languageId, $sortOrder, $widgetName, $widgetStatus);

        if($widgetId) {
            $this->session->data['success'][] = 'Виджет добавлен';
        }

        $this->redirectToMethod('index');
    }

    public function delete() {
        if(!$this->request->get['widget_id']) {
            $this->session->data['errors'][] = 'Что-то пошло не так... Нет такого виджета';
            $this->redirectToMethod('index');
        }

        $this->load->model('setting/setting');
        $this->load->model('extension/module/attributesWidget');
        $this->load->language('extension/module/attributesWidget');

        $this->data['language_id'] = $this->config->get('config_language_id');

        $deleted = $this->model_extension_module_attributesWidget->deleteWidget($this->request->get['widget_id']);

        if(!$deleted) {
            $this->session->data['errors'][] = 'Что-то пошло не так... попробуйте снова';
        } else {
            $this->session->data['success'][] = "Виджет успешно удален";
        }

        $this->redirectToMethod('index');
    }

    public function save()
    {
        if(!isset($this->request->post['status'])) {
            $this->session->data['errors'][] = 'Что-то пошло не так... попробуйте снова';
            $this->redirectToMethod('index');
        }

        $this->load->model('setting/setting');
        $this->load->model('extension/module/attributesWidget');
        $this->load->language('extension/module/attributesWidget');

        $this->data['language_id'] = $this->config->get('config_language_id');

        $settings['attributesWidget_status'] = $this->request->post['status'];
        $this->model_setting_setting->editSetting('attributesWidget', $settings);

        foreach($this->request->post['widgets'] as $widgetId => $widget) {
            $this->model_extension_module_attributesWidget->updateWidget($widget, $widgetId, $this->data['language_id']);
            $this->model_extension_module_attributesWidget->deactivateWidgetAttributes($widgetId);

            if(!isset($widget['attributes'])) {
                continue;
            }
            foreach ($widget['attributes'] as $attributeId => $value) {
                $this->model_extension_module_attributesWidget->activateWidgetAttribute($attributeId, $widgetId);
            }
        }

        $this->session->data['success'][] = "Настройки успешно сохранены";

        $this->redirectToMethod('index');
    }

    protected function validate()
    {
        $this->data['errors'] = array();

        if(!isset($this->request->post['new_widget_attribute_category']) || !$this->request->post['new_widget_attribute_category']) {
            $this->data['errors']['attributes_category'] = 'Выберете категорию';
        }

        if(!isset($this->request->post['attributes']) || !$this->request->post['attributes']) {
            $this->data['errors']['attributes'] = 'Выберете хотя бы один аттрибут';
        }

        if((int)$this->request->post['new_widget_sort_order'] <0 || (int)$this->request->post['new_widget_sort_order'] >100) {
            $this->data['errors']['attributes'] = 'Порядок сортировки должен находиться в значении от 0 до 100';
        }

        if(!$this->data['errors']) {
            return true;
        }
    }

    public function getAttributesByGroupId()
    {
        if (isset($this->request->post['new_widget_attribute_category']) && !empty($this->request->post['new_widget_attribute_category'])) {
            $this->load->model('catalog/attribute');
            $this->data['attributes'] = $this->model_catalog_attribute->getAttributes(array('filter_attribute_group_id' => $this->request->post['new_widget_attribute_category']));

            $this->load->model('design/layout');
            $this->data['layouts'] = $this->model_design_layout->getLayouts();
            $this->response->setOutput($this->load->view('extension/module/attributesList', $this->data));
        }
    }

    protected function redirectToMethod($url='')
    {
        $fullUrl = 'extension/module/attributesWidget';
        if ($url) {
            if (!strpos($url, '/')) {
                $url='/'.$url;
            }
            $fullUrl.=$url;
        }
        $redirect = $this->url->link(
            $fullUrl, 'token=' . $this->session->data['token']. '&type=module',
            true
        );

        $this->response->redirect($redirect);
    }

}