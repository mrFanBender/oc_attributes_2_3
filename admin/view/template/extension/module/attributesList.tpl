<?php foreach($attributes as $attribute): ?>
<div class="input-group">
    <span class="input-group-addon">
        <input type="checkbox" name="attributes[<?php echo $attribute['attribute_id'] ?>]" attribute_id="<?php echo $attribute['attribute_id'] ?>">
    </span>
    <span class="form-control"><?php echo $attribute['name'] ?></span>
</div>
<?php endforeach; ?>