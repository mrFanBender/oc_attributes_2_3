<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warnings) { ?>
        <?php foreach ($error_warnings as $error_name => $error_text) { ?>
            <div class="alert alert-warning"><?php echo $error_text; ?></div>
        <?php } ?>
    <?php } ?>
    <?php if ($info_messages) { ?>
    <?php foreach ($info_messages as $info_message) { ?>
    <div class="alert alert-success"><?php echo $info_message; ?></div>
    <?php } ?>
    <?php } ?>
        <div class="heading row">
                <h1 class="col-sm-12 col-md-9"><?php echo $text['heading_title']; ?></h1>
                <div class="buttons col-sm-12 col-md-3">
                    <a class="btn btn-default" href="<?php echo $cancel; ?>"><?= $text['button_cancel']; ?></a>
                    <button type="submit" class="btn btn-primary" form="form_html"><?= $text['button_save']; ?></>
                </div>
        </div>
        <hr/>
        <div class="row">
            <form action="<?php echo $create_new_widget; ?>" method="post" enctype="multipart/form-data" id="form_create_new_widget" class="form-horizontal">
                <label class="col-sm-2 col-md-2 status"><?php echo $text['text_new_widget'] ?></label>
                <div class="col-sm-12 col-md-10">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Категория аттрибутов</th>
                            <th>Аттрибуты</th>
                            <th>Порядок сортировки</th>
                            <th>Заголовок</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <select name="new_widget_attribute_category" id="new_widget_input_attribute_category" class="form-control" method="<?php echo $get_attributes_list; ?>">

                                    <?php foreach ($attribute_groups as $key => $attribute_group): ?>
                                        <option value="<?php echo $attribute_group['attribute_group_id'] ?>" <?php echo $key == 0 ? "selected='selected' " : " " ?> > <?php echo $attribute_group['name']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td id="new_widget_input_attribute_category_group">
                                <?php foreach ($new_widget_attributes as $new_widget_attribute) : ?>
                                <div class="input-group">
                                              <span class="input-group-addon">
                                                  <input type="checkbox" name="attributes[<?php echo $new_widget_attribute['attribute_id'] ?>]" attribute_id="<?php echo $new_widget_attribute['attribute_id'] ?>">
                                              </span>
                                    <span class="form-control"><?php echo $new_widget_attribute['name'] ?></span>
                                </div>
                                <?php endforeach; ?>
                            </td>
                            <td><input class="form-control" type="text" name="new_widget_sort_order"></td>
                            <td><input class="form-control" type="text" name="new_widget_name"></td>
                            <td>
                                <select name="new_widget_status" id="input_status" class="form-control col-sm-12 col-md-10">
                                    <option value="1" selected='selected'> <?php echo $this->data['statuses'][1]?></option>
                                    <option value="0"><?php echo $this->data['statuses'][0]?></option>
                                </select>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-success" form="form_create_new_widget"><?= $text['button_create_new_widget']; ?></>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>

        <hr/>
        <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="form_html" class="form-horizontal">
            <label class="col-sm-2 col-md-2 status"><?php echo $text['text_exist_widgets'] ?></label>
            <div class="col-sm-12 col-md-10">
                <?php if (!$widgets) { ?>
                    <p>У вас пока нет виджетов. Добавьте их</p>
                <?php } else { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Категория аттрибутов</th>
                        <th>Аттрибуты</th>
                        <th>Порядок сортировки</th>
                        <th>Заголовок</th>
                        <th>Статус</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($widgets as $widget): ?>
                            <tr>
                                <td>
                                    <label><?php echo $widget['name'] ?></label>
                                </td>
                                <td>
                                    <?php foreach ($widget['attributes'] as $attribute) : ?>
                                    <div class="input-group">
                                                  <span class="input-group-addon">
                                                      <input type="checkbox" name="widgets[<?php echo $widget['widget_id'] ?>][attributes][<?php echo $attribute['attribute_id'] ?>]" <?php echo $attribute['active'] ? 'checked' : '';  ?>>
                                                  </span>
                                        <span class="form-control"><?php echo $attribute['name'] ?></span>
                                    </div>
                                    <?php endforeach; ?>
                                </td>
                                <td><input class="form-control" type="text" name="<?php echo 'widgets['.$widget['widget_id'].'][sort_order]' ?>" value="<?= $widget['sort_order']?>"></td>
                                <td><input class="form-control" type="text" name="<?php echo 'widgets['.$widget['widget_id'].'][name]' ?>" value="<?= $widget['name']?>"></td>
                                <td>
                                    <select name="<?php echo 'widgets['.$widget['widget_id'].'][status]' ?>" class="form-control col-sm-12 col-md-10">
                                        <?php if ($widget['status'] == 1) { ?>
                                            <option value="1" selected="selected"> <?php echo $this->data['statuses'][1]?></option>
                                            <option value="0"><?php echo $this->data['statuses'][0]?></option>
                                        <?php } else { ?>
                                            <option value="1"> <?php echo $this->data['statuses'][1]?></option>
                                            <option value="0" selected="selected"><?php echo $this->data['statuses'][0]?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <a href="<?php echo $widget['delete_link']?>" class="btn btn-danger" widget_id="<?= $widget['widget_id'] ?>"><?= $text['button_delete']; ?></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <? } ?>
            </div>
            <div class="form-group">
                <label for="input_status" class="col-sm-12 col-md-2 status"><?php echo $text['text_status'] ?></label>
                <div class="col-sm-12 col-md-10">
                    <select name="status" id="input_status" class="form-control col-sm-12 col-md-10">
                            <option value="1" <?php echo ($status == 1 ? "selected='selected' " : " ")?> > <?php echo $this->data['statuses'][1]?></option>
                            <option value="0" <?php echo ($status == 0 ? "selected='selected' " : " ")?> > <?php echo $this->data['statuses'][0]?></option>
                    </select>
                </div>
            </div>
            <hr/>

        </form>

</div>
<?php echo $footer; ?>