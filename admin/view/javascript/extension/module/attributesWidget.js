
$(document).ready(function(){
    $("#new_widget_input_attribute_category").on('click', function(e, target){
        method = $(e.currentTarget).attr('method');
        groupId = e.currentTarget.value;
        $("#new_widget_input_attribute_category_group").load(
            method,
            {
                new_widget_attribute_category: groupId,
                param2: 2
            },
            function(data, status,) {
            }
        );
    });
});