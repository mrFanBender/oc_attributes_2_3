<?php


class ModelExtensionModuleAttributesWidget extends Model
{
    public function createTables()
    {
        $this->createAttributesWidgetTable();
        $this->createAttributeWidgetAttrTable();
        $this->createAttributesWidgetDescriptionTable();
    }

    public function createAttributesWidgetTable()
    {
        $query = $this->db->query("
            CREATE TABLE IF NOT EXISTS `".DB_PREFIX."attributes_widget` (
            `widget_id` INT AUTO_INCREMENT, 
            `attribute_group_id` INT NOT NULL, 
            `sort_order` INT,
            `status` BOOLEAN, 
            PRIMARY KEY(widget_id)) 
            DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
    }

    public function createAttributesWidgetDescriptionTable()
    {
        $query = $this->db->query("
            CREATE TABLE IF NOT EXISTS `".DB_PREFIX."attributes_widget_description` (
            `id` INT AUTO_INCREMENT,
            `widget_id` INT NOT NULL,
            `language_id` INT,  
            `name` VARCHAR(128), 
            PRIMARY KEY(id)) 
            DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
    }

    public function createAttributeWidgetAttrTable()
    {
        $query = $this->db->query("
            CREATE TABLE IF NOT EXISTS `".DB_PREFIX."attributes_widget_attr` (
            `id` INT AUTO_INCREMENT, 
            `widget_id` INT NOT NULL, 
            `attribute_id` INT NOT NULL,
            `active` BOOLEAN,
            PRIMARY KEY(id)) 
            DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
    }

    public function dropTables()
    {
        $this->dropAttributesWidgetTable();
        $this->dropAttributesWidgetAttrTable();
        $this->dropAttributesWidgetDescriptionTable();
    }

    public function dropAttributesWidgetTable()
    {
        $query = $this->db->query("
            DROP TABLE IF EXISTS `".DB_PREFIX."attributes_widget`");
    }

    public function dropAttributesWidgetDescriptionTable()
    {
        $query = $this->db->query("
            DROP TABLE IF EXISTS `".DB_PREFIX."attributes_widget_description`");
    }

    public function dropAttributesWidgetAttrTable()
    {
        $query = $this->db->query("
            DROP TABLE IF EXISTS `".DB_PREFIX."attributes_widget_attr`");
    }

    public function createWidget($attributeCategoryId, $attributes = array(), $languageId, $sortOrder, $name, $status)
    {
        $query = $this->db->query(
            sprintf("INSERT INTO `%sattributes_widget` (`attribute_group_id`, `sort_order`, `status`) 
            VALUES (%s, %s, %s)", DB_PREFIX, $attributeCategoryId, $sortOrder, $status));
        if (!$query) {
            return;
        }
        $widgetId = $this->db->getLastId();
        $this->createWidgetDescription($widgetId, $languageId, $name);
        $this->createWidgetAttributes($widgetId, $attributes);

        return $widgetId;
    }

    public function createWidgetDescription($widgetId, $languageId, $name) {
        $query = $this->db->query(
            sprintf("INSERT INTO `%sattributes_widget_description` (`widget_id`, `language_id`, `name`) 
            VALUES (%s, %s, '%s')", DB_PREFIX, $widgetId, $languageId, $name));
        if(!$query) {
            return;
        }

        $widgetDescriptionId = $this->db->getLastId();

        return $widgetDescriptionId;
    }

    public function createWidgetAttributes($widgetId, $attributes) {
        $values = '';
        foreach ($attributes as $attribute) {
            $values .= sprintf("(%s, %s, %s),", $widgetId, $attribute['attribute_id'], $attribute['active']);
        }

        $values = substr($values, 0, strlen($values)-1);

        $query = $this->db->query(
            sprintf("INSERT INTO `%sattributes_widget_attr` (`widget_id`, `attribute_id`, `active`) 
            VALUES %s", DB_PREFIX, $values));

        return $query;
    }

    public function getWidgets($languageId) {
        $widgets = $this->db->query(
            sprintf("SELECT awd.widget_id, `language_id`, `name`, aw.attribute_group_id, aw.sort_order, aw.status 
                FROM `%sattributes_widget_description` AS awd 
                LEFT JOIN `%sattributes_widget` AS aw
                ON aw.widget_id = awd.widget_id
                WHERE awd.language_id = %s
                ORDER BY sort_order ASC", DB_PREFIX,DB_PREFIX, $languageId))->rows;
        foreach($widgets as &$widget) {
            $attributes = $this->db->query(sprintf("SELECT * FROM `%sattributes_widget_attr`
                        WHERE `widget_id` = %s", DB_PREFIX, $widget['widget_id']))->rows;
            $widget['attributes'] = $attributes;
        }

        return $widgets;
    }

    public function getWidget($widgetId) {

    }

    public function deleteWidget($widgetId) {
        $query = $this->db->query(
            sprintf("DELETE aw.*, awd.*, awa.* FROM `%sattributes_widget` AS aw 
                LEFT JOIN `%sattributes_widget_description` AS awd 
                ON aw.widget_id = awd.widget_id
                LEFT JOIN `%sattributes_widget_attr` AS awa
                ON aw.widget_id = awa.widget_id
                WHERE aw.widget_id = %s", DB_PREFIX, DB_PREFIX, DB_PREFIX, $widgetId)
        );

        return $query;
    }

    public function updateWidget($widget, $widgetId, $languageId) {
        $query = $this->db->query(
            sprintf("UPDATE `%sattributes_widget` AS aw 
            INNER JOIN `%sattributes_widget_description` AS awd
            ON aw.widget_id = awd.widget_id
            SET aw.sort_order = %s, aw.status = %s, awd.name = '%s' 
            WHERE aw.widget_id = %s AND awd.language_id = %s",
                DB_PREFIX, DB_PREFIX, $widget['sort_order'], $widget['status'], $widget['name'], $widgetId, $languageId)
        );
    }

    public function deactivateWidgetAttributes($widgetId) {
        $query = $this->db->query(
            sprintf("UPDATE `%sattributes_widget_attr` 
            SET `active` = 0
            WHERE `widget_id` = %s",
                DB_PREFIX, $widgetId));

        return $query;
    }

    public function activateWidgetAttribute($attributeId, $widgetId) {
        $query = $this->db->query(
            sprintf("UPDATE `%sattributes_widget_attr` 
            SET `active` = 1
            WHERE `widget_id` = %s AND `attribute_id` = %s",
                DB_PREFIX, $widgetId, $attributeId));

        return $query;
    }



}