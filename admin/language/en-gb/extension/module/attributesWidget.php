<?php
$_['heading_title'] = 'Модуль виджета аттрибутов';
$_['attribute_widget_title'] = 'Модуль управления виджетами аттрибутов';
// Text
$_['text_module']             = 'Модули';
$_['text_success']            = 'Настройки успешно обновлены!';
$_['button_cancel']            =    "Отмена";
$_['button_save']            =    "Сохранить";

// Error
$_['error_permission']        = 'У Вас нет прав для изменения модуля "Виджет аттрибутов"!';

//Statuses
$_['status'][0]             =   'Off';
$_['status'][1]             =   'On';