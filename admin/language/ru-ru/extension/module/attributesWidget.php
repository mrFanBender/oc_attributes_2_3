<?php
     $_['heading_title'] = 'Модуль виджета аттрибутов';
    $_['attribute_widget_title'] = 'Модуль управления виджетами аттрибутов';
    // Text
    $_['text_home']             = 'Главная';
    $_['text_extension']             = 'Модули';
    $_['text_success']            = 'Настройки успешно обновлены!';
    $_['text_status']            =  'Статус модуля';
    $_['text_new_widget']            =  'Новый виджет';
    $_['text_exist_widgets']            =  'Ваши виджеты';
    $_['button_cancel']            =    "Отмена";
    $_['button_save']            =    "Сохранить";
    $_['button_delete']            =    "Удалить";
    $_['button_create_new_widget']      =   'Создать';
    // Error
    $_['error_permission']        = 'У Вас нет прав для изменения модуля "Виджет аттрибутов"!';

    //Statuses
    $_['status'][0]             =   'Отключен';
    $_['status'][1]             =   'Включен';
